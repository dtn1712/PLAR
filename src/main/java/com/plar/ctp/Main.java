package com.plar.ctp;

import com.plar.ctp.scheduler.JobManager;
import lombok.extern.slf4j.Slf4j;
import org.quartz.SchedulerException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;

import javax.mail.MessagingException;

@Slf4j
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@ImportResource({"classpath:spring/beanconfig/application-context.xml"})
public class Main {

	private static ApplicationContext context;

	public static void main(String[] args) throws SchedulerException, MessagingException {
		context = SpringApplication.run(Main.class, args);
		JobManager jobManager = context.getBean(JobManager.class);
		jobManager.load();
		jobManager.startScheduleJob();
	}

	public static ApplicationContext getContext() {
		return context;
	}


}
