package com.plar.ctp.scheduler;

import com.plar.ctp.Main;
import com.plar.ctp.domain.dao.ArbitrageDAO;
import com.plar.ctp.domain.model.filtered.Arbitrage;
import com.plar.ctp.exchange.ExchangeFactory;
import com.plar.ctp.exchange.impl.PoloniexExchange;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class FilterHighestArbitrageJob implements Job {

    private static final int PERIOD_IN_SECOND = 20;

    private ArbitrageDAO arbitrageDAO;
    private ExchangeFactory exchangeFactory;

    public FilterHighestArbitrageJob() {
        this.arbitrageDAO = Main.getContext().getBean(ArbitrageDAO.class);
        this.exchangeFactory = Main.getContext().getBean(ExchangeFactory.class);
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        DateTime start = DateTime.now().minusSeconds(PERIOD_IN_SECOND);
        DateTime end = DateTime.now();
        List<Arbitrage> arbitrages = arbitrageDAO.listArbitrageDuringPeriod(exchangeFactory.getExchange(PoloniexExchange.class), start, end);

        Map<String, List<Arbitrage>> arbitrageMap = new HashMap<>();
        Map<String, Integer> arbitrageCountMap = new HashMap<>();
        for (Arbitrage arbitrage : arbitrages) {
            String chain = arbitrage.getChain();
            int count = (arbitrageCountMap.containsKey(chain)) ? arbitrageCountMap.get(chain) : 0;
            arbitrageCountMap.put(chain, count + 1);

            List<Arbitrage> arbitrageList = (arbitrageMap.containsKey(chain)) ? arbitrageMap.get(chain) : new ArrayList<>();
            arbitrageList.add(arbitrage);
            arbitrageMap.put(chain, arbitrageList);
        }

        int max = 0;
        for (Map.Entry<String, Integer> entry : arbitrageCountMap.entrySet()) {
            if (max < entry.getValue()) {
                max = entry.getValue();
            }
        }

        List<Arbitrage> deletedArbitrages = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : arbitrageCountMap.entrySet()) {
            if (max != entry.getValue()) {
                deletedArbitrages.addAll(arbitrageMap.get(entry.getKey()));
            }
        }

        arbitrageDAO.deleteArbitrages(deletedArbitrages);
    }

}
