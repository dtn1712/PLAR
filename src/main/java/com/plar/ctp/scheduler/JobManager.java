package com.plar.ctp.scheduler;


import com.plar.ctp.Main;
import com.plar.ctp.domain.dao.JobDAO;
import com.plar.ctp.domain.model.Job;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

@Slf4j
@Component
public class JobManager {

    private Scheduler scheduler;
    private List<Job> allJobs = new ArrayList<>();

    @Autowired
    public JobManager() throws SchedulerException {
        SchedulerFactory sf = new StdSchedulerFactory();
        scheduler = sf.getScheduler();
    }

    public void load() {
        JobDAO jobDAO = Main.getContext().getBean(JobDAO.class);
        allJobs = jobDAO.loadAll();
    }

    public void startScheduleJob() throws SchedulerException {
        for (Job job : allJobs) {
            try {
                Class clazz = Class.forName(job.getClassName());
                JobDetail jobDetail = createJobDetail(clazz, job.getJobName(), job.getGroup());
                CronTrigger cronTrigger = createTrigger(job.getSchedule(), job.getTrigger(), job.getGroup());
                Date ft = scheduler.scheduleJob(jobDetail, cronTrigger);
                log.info(job.getKey() + " has been scheduled to run at: " + ft
                        + " and repeat based on expression: "
                        + cronTrigger.getCronExpression());
            } catch (Exception e) {
                log.error("Failed to schedule job " + job.getJobName(), e);
            }
        }

        scheduler.start();
    }

    public void rescheduleJob() {
        for (Job job : allJobs) {
            try {
                CronTrigger newTrigger = createTrigger(job.getSchedule(), job.getTrigger(), job.getGroup());
                scheduler.rescheduleJob(TriggerKey.triggerKey(job.getTrigger(), job.getGroup()), newTrigger);
            } catch (Exception e) {
                log.error("Failed to reschedule job " + job.getJobName(), e);
            }
        }
    }

    public void shutDown() throws SchedulerException {
        scheduler.shutdown();
    }

    private JobDetail createJobDetail(Class clazz, String jobName, String group) {
        return newJob(clazz)
                .withIdentity(jobName, group)
                .build();
    }

    private CronTrigger createTrigger(String jobSchedule, String triggerName, String group) throws SchedulerException {
        return newTrigger()
                .withIdentity(triggerName, group)
                .withSchedule(cronSchedule(jobSchedule).inTimeZone(TimeZone.getDefault()))
                .build();
    }
}
