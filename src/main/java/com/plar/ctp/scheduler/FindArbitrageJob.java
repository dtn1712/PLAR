package com.plar.ctp.scheduler;

import com.plar.ctp.Main;
import com.plar.ctp.analyzer.ArbitrageAnalyzer;
import com.plar.ctp.analyzer.OrderBookAnalyzer;
import com.plar.ctp.domain.dto.ArbitrageDTO;
import com.plar.ctp.domain.model.OrderBook;
import com.plar.ctp.domain.model.filtered.Arbitrage;
import com.plar.ctp.domain.model.filtered.ArbitragePair;
import com.plar.ctp.domain.model.raw.Ticker;
import com.plar.ctp.exchange.Exchange;
import com.plar.ctp.exchange.ExchangeFactory;
import com.plar.ctp.exchange.impl.PoloniexExchange;
import com.plar.ctp.service.DataService;
import com.plar.ctp.util.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class FindArbitrageJob implements Job {

    private RabbitTemplate rabbitTemplate;
    private DataService dataService;
    private ExchangeFactory exchangeFactory;
    private ArbitrageAnalyzer arbitrageAnalyzer;
    private OrderBookAnalyzer orderBookAnalyzer;

    public FindArbitrageJob() {
        this.rabbitTemplate = Main.getContext().getBean(RabbitTemplate.class);
        this.dataService = Main.getContext().getBean(DataService.class);
        this.exchangeFactory = Main.getContext().getBean(ExchangeFactory.class);
        this.arbitrageAnalyzer = Main.getContext().getBean(ArbitrageAnalyzer.class);
        this.orderBookAnalyzer = Main.getContext().getBean(OrderBookAnalyzer.class);
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Exchange exchange = exchangeFactory.getExchange(PoloniexExchange.class);

        List<Ticker> tickers = exchange.getTickers();
        List<OrderBook> orderBooks = exchange.getOrderBooks();

        try {
            List<Arbitrage> arbitrages = arbitrageAnalyzer.analyze(exchange, dataService.createArbitragePairs(tickers));

            dataService.setupArbitrageOrderBooks(arbitrages, orderBooks);

            orderBookAnalyzer.analyze(arbitrages);
//            printOrderBook(arbitrages);

            rabbitTemplate.convertAndSend(MapperUtils.getObjectMapper().writeValueAsString(toDTO(arbitrages)));
        } catch (Exception e) {
            log.error("Failed to produce message", e);
        }
    }

    private List<ArbitrageDTO> toDTO(List<Arbitrage> arbitrages) {
        return arbitrages.stream().map(Arbitrage::toDTO).collect(Collectors.toList());
    }

    private void printOrderBook(List<Arbitrage> arbitrages) {
        for (Arbitrage arbitrage : arbitrages) {
            String content = "";
            for (Map.Entry<ArbitragePair, OrderBook> entry : arbitrage.getOrderBooks().entrySet()) {
                OrderBook orderBook = entry.getValue();
                content = content + orderBook.getTradeCurrency() + "_" +orderBook.getMainCurrency() + "=>ask:(" +
                                orderBook.getAsks().get(0).getPrice() + "," + orderBook.getAsks().get(0).getQuantity() + "), bid:(" +
                                orderBook.getBids().get(0).getPrice() + "," + orderBook.getBids().get(0).getQuantity() + ")\n";

            }
            System.out.println(arbitrage.getChain() + ": " + arbitrage.getRate());
            System.out.println(content + "\n");

        }
    }
}
