package com.plar.ctp.domain.dto;

import com.plar.ctp.Main;
import com.plar.ctp.domain.model.filtered.ArbitragePair;
import com.plar.ctp.exchange.ExchangeFactory;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

@Getter
@Setter
public class ArbitragePairDTO {

    private String currencyBuy;
    private String currencySell;
    private Double rate;
    private String exchange;
    private Double rateAfterFee;
    private DateTime lastUpdatedTime;

    public ArbitragePair toModel() {
        ExchangeFactory exchangeFactory = Main.getContext().getBean(ExchangeFactory.class);
        return new ArbitragePair(currencyBuy, currencySell, rate, exchangeFactory.getExchange(exchange), rateAfterFee, lastUpdatedTime);
    }
}
