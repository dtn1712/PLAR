package com.plar.ctp.domain.dto;

import com.plar.ctp.domain.model.OrderBook;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class OrderBookDTO {

    private List<OrderDTO> asks;
    private List<OrderDTO> bids;
    private String mainCurrency;        // The currency that will be list in the ask and bids
    private String tradeCurrency;       // The current that will be used to trade
    private DateTime lastUpdatedTime;

    public OrderBook toModel() {
        OrderBook orderBook = new OrderBook();
        orderBook.setAsks(asks.stream().map(OrderDTO::toModel).collect(Collectors.toList()));
        orderBook.setBids(bids.stream().map(OrderDTO::toModel).collect(Collectors.toList()));
        orderBook.setMainCurrency(mainCurrency);
        orderBook.setTradeCurrency(tradeCurrency);
        orderBook.setLastUpdatedTime(lastUpdatedTime);
        return orderBook;
    }

}
