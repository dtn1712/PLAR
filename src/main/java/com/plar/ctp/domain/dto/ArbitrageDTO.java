package com.plar.ctp.domain.dto;

import com.plar.ctp.Main;
import com.plar.ctp.domain.model.OrderBook;
import com.plar.ctp.domain.model.filtered.Arbitrage;
import com.plar.ctp.domain.model.filtered.ArbitragePair;
import com.plar.ctp.exchange.ExchangeFactory;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@Setter
public class ArbitrageDTO {

    private long id;
    private List<ArbitragePairDTO> pairs;
    private Map<ArbitragePairDTO, OrderBookDTO> orderBooks;
    private double rate;
    private DateTime lastUpdatedTime;
    private String exchange;

    public Arbitrage toModel() {
        ExchangeFactory exchangeFactory = Main.getContext().getBean(ExchangeFactory.class);
        Arbitrage arbitrage = new Arbitrage();
        arbitrage.setId(id);
        arbitrage.setExchange(exchangeFactory.getExchange(exchange));
        arbitrage.setLastUpdatedTime(lastUpdatedTime);
        arbitrage.setRate(rate);
        arbitrage.setPairs(pairs.stream().map(ArbitragePairDTO::toModel).collect(Collectors.toList()));
        arbitrage.setOrderBooks(convertOrderBooksToModel());
        return arbitrage;
    }

    public Map<ArbitragePair, OrderBook> convertOrderBooksToModel() {
        Map<ArbitragePair, OrderBook> orderBooks = new HashMap<>();
        for (Map.Entry<ArbitragePairDTO, OrderBookDTO> entry : this.orderBooks.entrySet()) {
            orderBooks.put(entry.getKey().toModel(), entry.getValue().toModel());
        }
        return orderBooks;
    }
}
