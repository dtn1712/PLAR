package com.plar.ctp.domain.dto;

import com.plar.ctp.domain.model.Order;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderDTO {

    private Double price;
    private Double quantity;

    public Order toModel() {
        Order order = new Order();
        order.setPrice(price);
        order.setQuantity(quantity);
        return order;
    }
}
