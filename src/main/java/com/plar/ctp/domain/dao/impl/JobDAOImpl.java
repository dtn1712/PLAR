package com.plar.ctp.domain.dao.impl;

import com.plar.ctp.domain.dao.BaseDAO;
import com.plar.ctp.domain.dao.JobDAO;
import com.plar.ctp.domain.model.Job;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Slf4j
@Repository
public class JobDAOImpl extends BaseDAO implements JobDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Job> loadAll() {
        String sql = "SELECT * FROM jobs WHERE isActive=1";
        return jdbcTemplate.query(sql, new JobMapper());
    }

    @Override
    public Job getJobByKey(String key) {
        String sql = "SELECT * FROM jobs WHERE key=?";
        return (Job) queryForObject(jdbcTemplate, sql, new Object[] {key}, new JobMapper());
    }

    public static class JobMapper implements RowMapper<Job> {

        @Override
        public Job mapRow(ResultSet resultSet, int i) throws SQLException {
            Job job = new Job();
            job.setKey(resultSet.getString("key"));
            job.setClassName(resultSet.getString("className"));
            job.setDescription(resultSet.getString("description"));
            job.setGroup(resultSet.getString("group"));
            job.setJobName(resultSet.getString("jobName"));
            job.setSchedule(resultSet.getString("schedule"));
            job.setTrigger(resultSet.getString("trigger"));
            job.setActive(resultSet.getBoolean("isActive"));
            return job;
        }
    }
}
