package com.plar.ctp.domain.dao.impl;

import com.plar.ctp.domain.dao.ArbitrageDAO;
import com.plar.ctp.domain.model.filtered.Arbitrage;
import com.plar.ctp.domain.model.filtered.ArbitragePair;
import com.plar.ctp.exchange.Exchange;
import com.plar.ctp.exchange.ExchangeFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ArbitrageDAOImpl implements ArbitrageDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ExchangeFactory exchangeFactory;

    @Override
    public void saveArbitrage(Arbitrage arbitrage) {

    }

    @Override
    public void saveArbitrages(List<Arbitrage> arbitrages) {

    }

    @Override
    public void createArbitrages(List<Arbitrage> arbitrages) {
        String insertArbitrageSql = "INSERT INTO arbitrages (rate, chain, exchange, createdAt, updatedAt) VALUES (?,?,?,NOW(),NOW())";
        for (Arbitrage arbitrage : arbitrages) {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    connection -> {
                        PreparedStatement ps = connection.prepareStatement(insertArbitrageSql, new String[]{"id"});
                        ps.setDouble(1, arbitrage.getRate());
                        ps.setString(2, arbitrage.getChain());
                        ps.setString(3, arbitrage.getExchange().getClass().getSimpleName());
                        return ps;
                    },
                    keyHolder);

            int arbitrageId = keyHolder.getKey().intValue();

            List<ArbitragePair> pairs = arbitrage.getPairs();
            String insertArbitragePairSql = "INSERT INTO arbitrage_pairs (currencyBuy, currencySell, arbitrageId, chainPosition, rate, rateAfterFee, exchange, createdAt, updatedAt) VALUES (?,?,?,?,?,?,?,NOW(),NOW())";

            jdbcTemplate.batchUpdate(insertArbitragePairSql, new BatchPreparedStatementSetter() {

                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    ArbitragePair arbitragePair = pairs.get(i);
                    ps.setString(1, arbitragePair.getCurrencyBuy());
                    ps.setString(2, arbitragePair.getCurrencySell());
                    ps.setLong(3, arbitrageId);
                    ps.setInt(4, i);
                    ps.setDouble(5, arbitragePair.getRate());
                    ps.setDouble(6, arbitragePair.getRateAfterFee());
                    ps.setString(7, arbitragePair.getExchange().getClass().getSimpleName());
                }

                @Override
                public int getBatchSize() {
                    return pairs.size();
                }
            });
        }
    }

    @Override
    public void createArbitrage(Arbitrage arbitrage) {

    }

    @Override
    public void deleteArbitrage(Arbitrage arbitrage) {

    }

    @Override
    public void deleteArbitrages(List<Arbitrage> arbitrages) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());

        String deletedArbitrageSql = "DELETE FROM arbitrages WHERE id IN (:arbitrageIds)";
        String deletePairSql = "DELETE FROM arbitrage_pairs WHERE arbitrageId IN (:arbitrageIds)";

        List<Long> arbitrageIds = arbitrages.stream().map(Arbitrage::getId).collect(Collectors.toList());
        if (!arbitrageIds.isEmpty()) {
            Map params = new HashMap<>();
            params.put("arbitrageIds", arbitrageIds);
            namedParameterJdbcTemplate.update(deletedArbitrageSql, params);
            namedParameterJdbcTemplate.update(deletePairSql, params);
        }
    }

    @Override
    public List<Arbitrage> listArbitrageDuringPeriod(Exchange exchange, DateTime start, DateTime end) {
        String sql = "SELECT * FROM arbitrages WHERE exchange=? AND createdAt > ? AND createdAt < ? ORDER BY createdAt ASC";
        List<Arbitrage> arbitrages = jdbcTemplate.query(sql, new Object[]{exchange.getClass().getSimpleName(), new Timestamp(start.getMillis()), new Timestamp(end.getMillis())}, new ArbitrageMapper(exchangeFactory));

        for (Arbitrage arbitrage : arbitrages) {
            String subSql = "SELECT ap.* FROM arbitrage_pairs ap INNER JOIN arbitrages a ON a.id = ap.arbitrageId WHERE a.id = ? ORDER BY ap.chainPosition ASC";
            List<ArbitragePair> pairs = jdbcTemplate.query(subSql, new Object[] {arbitrage.getId()}, new PairMapper(exchangeFactory));
            arbitrage.setPairs(pairs);
        }

        return arbitrages;
    }

    public static class PairMapper implements RowMapper<ArbitragePair> {

        private ExchangeFactory exchangeFactory;

        public PairMapper(ExchangeFactory exchangeFactory) {
            this.exchangeFactory = exchangeFactory;
        }

        @Override
        public ArbitragePair mapRow(ResultSet resultSet, int i) throws SQLException {
            ArbitragePair pair = new ArbitragePair(resultSet.getString("currencyBuy"), resultSet.getString("currencySell"),
                    resultSet.getDouble("rate"), exchangeFactory.getExchange(resultSet.getString("exchange")),
                    resultSet.getDouble("rateAfterFee"), new DateTime(resultSet.getTimestamp("updatedAt")));
            pair.setId(resultSet.getLong("id"));
            return pair;
        }
    }

    public static class ArbitrageMapper implements RowMapper<Arbitrage> {

        private ExchangeFactory exchangeFactory;

        public ArbitrageMapper(ExchangeFactory exchangeFactory) {
            this.exchangeFactory = exchangeFactory;
        }

        @Override
        public Arbitrage mapRow(ResultSet resultSet, int i) throws SQLException {
            Arbitrage arbitrage = new Arbitrage();
            arbitrage.setId(resultSet.getLong("id"));
            arbitrage.setExchange(exchangeFactory.getExchange(resultSet.getString("exchange")));
            arbitrage.setRate(resultSet.getDouble("rate"));
            arbitrage.setLastUpdatedTime(new DateTime(resultSet.getTimestamp("updatedAt")));
            return arbitrage;
        }
    }
}
