package com.plar.ctp.domain.dao;

import com.plar.ctp.domain.model.filtered.Arbitrage;
import com.plar.ctp.exchange.Exchange;
import org.joda.time.DateTime;

import java.util.List;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 1/17/18
 * Time: 6:11 AM
 */
public interface ArbitrageDAO {

    void saveArbitrage(Arbitrage arbitrage);

    void saveArbitrages(List<Arbitrage> arbitrages);

    void createArbitrages(List<Arbitrage> arbitrages);

    void createArbitrage(Arbitrage arbitrage);

    void deleteArbitrage(Arbitrage arbitrage);

    void deleteArbitrages(List<Arbitrage> arbitrages);

    List<Arbitrage> listArbitrageDuringPeriod(Exchange exchange, DateTime start, DateTime end);
}
