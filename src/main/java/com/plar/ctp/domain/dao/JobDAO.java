package com.plar.ctp.domain.dao;

import com.plar.ctp.domain.model.Job;

import java.util.List;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 12/22/17
 * Time: 1:57 PM
 */
public interface JobDAO {

    List<Job> loadAll();

    Job getJobByKey(String key);
}
