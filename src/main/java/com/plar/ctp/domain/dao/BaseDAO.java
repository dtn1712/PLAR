package com.plar.ctp.domain.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

@Slf4j
public abstract class BaseDAO {

    protected Object queryForObject(JdbcTemplate jdbcTemplate, String sql, Object[] args, Class requiredType, Object defaultValueIfNotFound) {
        try {
            return jdbcTemplate.queryForObject(sql, args, requiredType);
        } catch (EmptyResultDataAccessException e) {
            log.info(String.format("There is no object found for sql query : \n%s", sql));
        } catch (Exception e) {
            log.error(String.format("Failed to query for object with sql query:\n%s", sql), e);
        }
        return defaultValueIfNotFound;
    }

    protected Object queryForObject(JdbcTemplate jdbcTemplate, String sql, Object[] args, RowMapper rowMapper) {
        try {
            return jdbcTemplate.queryForObject(sql, args, rowMapper);
        } catch (EmptyResultDataAccessException e) {
            log.info(String.format("There is no object found for sql query : \n%s", sql));
        } catch (Exception e) {
            log.error(String.format("Failed to query for object with sql query:\n%s", sql), e);
        }
        return null;
    }

}
