package com.plar.ctp.domain.model;

import com.plar.ctp.domain.dto.OrderBookDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderBook {

    private List<Order> asks;
    private List<Order> bids;
    private String mainCurrency;        // The currency that will be list in the ask and bids
    private String tradeCurrency;       // The current that will be used to trade
    private DateTime lastUpdatedTime;

    public OrderBookDTO toDTO() {
        OrderBookDTO orderBookDTO = new OrderBookDTO();
        orderBookDTO.setAsks(asks.stream().map(Order::toDTO).collect(Collectors.toList()));
        orderBookDTO.setBids(bids.stream().map(Order::toDTO).collect(Collectors.toList()));
        orderBookDTO.setMainCurrency(mainCurrency);
        orderBookDTO.setTradeCurrency(tradeCurrency);
        orderBookDTO.setLastUpdatedTime(lastUpdatedTime);
        return orderBookDTO;
    }
}
