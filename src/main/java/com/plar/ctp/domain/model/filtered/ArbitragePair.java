package com.plar.ctp.domain.model.filtered;

import com.plar.ctp.domain.dto.ArbitragePairDTO;
import com.plar.ctp.exchange.Exchange;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

@Getter
@Setter
public class ArbitragePair {

    private Long id;
    private String currencyBuy;
    private String currencySell;
    private Double rate;
    private Exchange exchange;
    private Double rateAfterFee;
    private DateTime lastUpdatedTime;

    public ArbitragePair(String currencyBuy, String currencySell, Double rate, Exchange exchange) {
        this.currencyBuy = currencyBuy;
        this.currencySell = currencySell;
        this.rate = rate;
        this.exchange = exchange;
        this.rateAfterFee = rate * (1.0 - exchange.getFee());
        this.lastUpdatedTime = DateTime.now();
    }

    public ArbitragePair(String currencyBuy, String currencySell, Double rate, Exchange exchange, Double rateAfterFee, DateTime lastUpdatedTime) {
        this.currencyBuy = currencyBuy;
        this.currencySell = currencySell;
        this.rate = rate;
        this.exchange = exchange;
        this.rateAfterFee = rateAfterFee;
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public ArbitragePairDTO toDTO() {
        ArbitragePairDTO arbitragePairDTO = new ArbitragePairDTO();
        arbitragePairDTO.setCurrencyBuy(currencyBuy);
        arbitragePairDTO.setCurrencySell(currencySell);
        arbitragePairDTO.setRate(rate);
        arbitragePairDTO.setRateAfterFee(rateAfterFee);
        arbitragePairDTO.setExchange(exchange.getClass().getName());
        arbitragePairDTO.setLastUpdatedTime(lastUpdatedTime);
        return arbitragePairDTO;
    }

}
