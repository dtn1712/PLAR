package com.plar.ctp.domain.model.filtered;

import com.plar.ctp.domain.dto.ArbitrageDTO;
import com.plar.ctp.domain.dto.OrderBookDTO;
import com.plar.ctp.domain.dto.ArbitragePairDTO;
import com.plar.ctp.domain.model.OrderBook;
import com.plar.ctp.exchange.Exchange;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Arbitrage {

    private long id;
    private List<ArbitragePair> pairs;
    private Map<ArbitragePair, OrderBook> orderBooks;
    private double rate;
    private DateTime lastUpdatedTime;
    private Exchange exchange;

    public ArbitrageDTO toDTO() {
        ArbitrageDTO arbitrageDTO = new ArbitrageDTO();
        arbitrageDTO.setId(id);
        arbitrageDTO.setRate(rate);
        arbitrageDTO.setLastUpdatedTime(lastUpdatedTime);
        arbitrageDTO.setExchange(exchange.getClass().getName());
        arbitrageDTO.setPairs(pairs.stream().map(ArbitragePair::toDTO).collect(Collectors.toList()));
        arbitrageDTO.setOrderBooks(convertOrderBooksToDTO());
        return arbitrageDTO;
    }

    private Map<ArbitragePairDTO, OrderBookDTO> convertOrderBooksToDTO() {
        Map<ArbitragePairDTO, OrderBookDTO> orderBooks = new HashMap<>();
        for (Map.Entry<ArbitragePair, OrderBook> entry : this.orderBooks.entrySet()) {
            orderBooks.put(entry.getKey().toDTO(), entry.getValue().toDTO());
        }
        return orderBooks;
    }

    public String getChain() {
        String content = "";
        for (ArbitragePair pair : pairs) {
            content = content + pair.getCurrencySell() + "_" + pair.getCurrencyBuy() + ",";
        }
        return content.substring(0, content.length()-1);
    }
}
