package com.plar.ctp.domain.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Job {

    private String key;
    private String className;
    private String jobName;
    private String description;
    private String group;
    private String schedule;
    private String trigger;
    private boolean isActive;

}
