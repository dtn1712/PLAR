package com.plar.ctp.domain.model;

import com.plar.ctp.domain.dto.OrderDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Order {

    private Double price;
    private Double quantity;

    public OrderDTO toDTO() {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setPrice(price);
        orderDTO.setQuantity(quantity);
        return orderDTO;
    }
}
