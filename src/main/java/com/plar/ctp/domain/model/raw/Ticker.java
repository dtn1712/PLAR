package com.plar.ctp.domain.model.raw;

import com.plar.ctp.exchange.Exchange;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Ticker {

    private String name;
    private double last;
    private double lowestAsk;
    private double highestBid;
    private double percentChange;
    private double baseVolume;
    private double quoteVolume;
    private double high24Hr;
    private double low24Hr;
    private Exchange exchange;
}
