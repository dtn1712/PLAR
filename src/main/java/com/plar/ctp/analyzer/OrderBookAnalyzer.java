package com.plar.ctp.analyzer;

import com.plar.ctp.domain.model.Order;
import com.plar.ctp.domain.model.OrderBook;
import com.plar.ctp.domain.model.filtered.Arbitrage;
import com.plar.ctp.domain.model.filtered.ArbitragePair;
import com.plar.ctp.exchange.Exchange;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderBookAnalyzer {

    public List<Order> analyze(List<Arbitrage> arbitrages) {
        for (Arbitrage arbitrage : arbitrages) {
            ArbitragePair pair1 = arbitrage.getPairs().get(0);
            ArbitragePair pair2 = arbitrage.getPairs().get(1);
            ArbitragePair pair3 = arbitrage.getPairs().get(2);

            OrderBook orderBook1 = arbitrage.getOrderBooks().get(pair1);
            OrderBook orderBook2 = arbitrage.getOrderBooks().get(pair2);
            OrderBook orderBook3 = arbitrage.getOrderBooks().get(pair3);

            Double newRate = findNewRateWithOrderBook1(arbitrage.getExchange(), orderBook1, orderBook2, orderBook3, pair2.getRate(), pair3.getRate());

            String content = orderBook1.getTradeCurrency() + "_" + orderBook1.getMainCurrency() + "," +
                    orderBook2.getTradeCurrency() + "_" + orderBook2.getMainCurrency() + "," +
                    orderBook3.getTradeCurrency() + "_" + orderBook3.getMainCurrency() + ":" + arbitrage.getRate() + "," + newRate;

            System.out.println(content);
//
//            OrderBook baseOrderBook;
//            Double quantity1 = orderBook1.getAsks().get(0).getQuantity();
//            Double quantity2 = orderBook2.getAsks().get(0).getQuantity();
//            Double quantity3 = orderBook3.getBids().get(0).getQuantity();
//
//            if (quantity1 <= quantity2 ) {
//                quantity3 = orderBook3.getBids().get(0).getQuantity() * orderBook3.getBids().get(0).getPrice();
//                quantity1 = orderBook1.getAsks().get(0).getQuantity() * orderBook1.getAsks().get(0).getPrice();
//                if (quantity3 > quantity1) {
//                    baseOrderBook = orderBook1;
//                } else {
//                    baseOrderBook = orderBook3;
//                }
//            } else {
//                quantity2 = orderBook2.getAsks().get(0).getQuantity() * orderBook2.getAsks().get(0).getPrice();
//                if (quantity3 > quantity2) {
//                    baseOrderBook = orderBook2;
//                } else {
//                    baseOrderBook = orderBook3;
//                }
//            }

        }

        return null;
    }

    private Double findNewRateWithOrderBook1(Exchange exchange, OrderBook orderBook1, OrderBook orderBook2, OrderBook orderBook3, Double oldRate2, Double oldRate3) {
        Double baseQuantity = orderBook1.getAsks().get(0).getQuantity();

        Double total2 = 0.0;
        Double quantity2 = 0.0;

        for (Order order : orderBook2.getAsks()) {
            if (quantity2 < baseQuantity) {
                quantity2 = quantity2 + order.getQuantity();
                total2 = total2 + (order.getQuantity() * order.getPrice());
            } else {
                break;
            }
        }

        if (quantity2 < baseQuantity) {
            baseQuantity = quantity2;
        }

        Double total3 = 0.0;
        Double quantity3 = 0.0;

        for (Order order : orderBook3.getBids()) {
            if (quantity3 < baseQuantity) {
                quantity3 = quantity3 + order.getQuantity();
                total3 = total3 + (order.getQuantity() * order.getPrice());
            } else {
                break;
            }
        }

        Double rate1 = orderBook1.getAsks().get(0).getPrice();
        Double rate2 = compareAndChooseClosestRate(oldRate2, total2 / quantity2);
        Double rate3 = compareAndChooseClosestRate(oldRate3, total3 / quantity3);

        Double rateAfterFee = (1.0 - exchange.getFee());
        return rate1 * rateAfterFee * rate2 * rateAfterFee * rate3 * rateAfterFee;
    }

    private Double compareAndChooseClosestRate(Double oldRate, Double newRate) {
        Double diff1 = Math.abs(oldRate - newRate);
        Double diff2 = Math.abs(oldRate - (1/ newRate));
        if (diff1 < diff2) {
            return newRate;
        } else {
            return 1 / newRate;
        }
    }
}
