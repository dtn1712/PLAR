package com.plar.ctp.analyzer;

import com.plar.ctp.Constants;
import com.plar.ctp.domain.model.filtered.Arbitrage;
import com.plar.ctp.domain.model.filtered.ArbitragePair;
import com.plar.ctp.exchange.Exchange;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class ArbitrageAnalyzer {

    public List<Arbitrage> analyze(Exchange exchange, List<ArbitragePair> inputPairs) {
        List<ArbitragePair> buyBtcList = new ArrayList<>();
        List<ArbitragePair> noBtcList = new ArrayList<>();
        List<ArbitragePair> sellBtcList = new ArrayList<>();
        for (ArbitragePair pair : inputPairs) {
            if (pair.getCurrencyBuy().equals(Constants.BASE_COIN)) {
                buyBtcList.add(pair);
            } else if (pair.getCurrencySell().equals(Constants.BASE_COIN)) {
                sellBtcList.add(pair);
            } else {
                noBtcList.add(pair);
            }
        }

        List<Arbitrage> arbitrages = new ArrayList<>();
        for (ArbitragePair sellBtcPair : sellBtcList) {
            noBtcList.stream().filter(noBtcPair -> sellBtcPair.getCurrencyBuy().equals(noBtcPair.getCurrencySell())).forEach(noBtcPair -> {
                buyBtcList.stream().filter(buyBtcPair -> noBtcPair.getCurrencyBuy().equals(buyBtcPair.getCurrencySell())).forEach(buyBtcPair -> {
                    double rate = sellBtcPair.getRateAfterFee() * noBtcPair.getRateAfterFee() * buyBtcPair.getRateAfterFee();
                    if (rate > 1.0) {
                        Arbitrage arbitrage = new Arbitrage();
                        arbitrage.setPairs(Arrays.asList(sellBtcPair, noBtcPair, buyBtcPair));
                        arbitrage.setRate(rate);
                        arbitrage.setLastUpdatedTime(DateTime.now());
                        arbitrage.setExchange(exchange);
                        arbitrages.add(arbitrage);
                    }
                });
            });
        }

        Collections.sort(arbitrages, (o1, o2) -> {
            if (o1.getRate() > o2.getRate()) {
                return 1;
            } else if (o1.getRate() < o2.getRate()) {
                return -1;
            }
            return 0;
        });

        return arbitrages.subList(arbitrages.size() - Constants.MAX_TOTAL_ARBITRAGE_CHOICES, arbitrages.size());
    }
}
