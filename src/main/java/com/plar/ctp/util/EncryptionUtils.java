package com.plar.ctp.util;

import lombok.extern.slf4j.Slf4j;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import static javax.crypto.Cipher.DECRYPT_MODE;
import static javax.crypto.Cipher.ENCRYPT_MODE;

@Slf4j
public class EncryptionUtils {

    public static byte[] decrypt(byte[] key, byte[] encryptData) {
        if (encryptData == null || key == null) {
            return null;
        }

        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(DECRYPT_MODE, new SecretKeySpec(key, "AES"), new IvParameterSpec(key));
            byte[] result = cipher.doFinal(encryptData);
            return result;
        } catch (Exception e) {
            log.error("Failed to encrypt", e);
        }

        return null;
    }

    public static byte[] encrypt(byte[] key, byte[] rawData) {
        if (rawData == null || key == null) {
            return null;
        }

        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(ENCRYPT_MODE, new SecretKeySpec(key, "AES"), new IvParameterSpec(key));
            byte[] result = cipher.doFinal(rawData);
            return result;
        } catch (Exception e) {
            log.error("Failed to encrypt", e);
        }

        return null;
    }
}
