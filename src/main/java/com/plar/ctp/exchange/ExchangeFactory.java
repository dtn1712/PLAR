package com.plar.ctp.exchange;


import com.plar.ctp.Main;
import com.plar.ctp.exchange.impl.PoloniexExchange;
import org.springframework.stereotype.Component;

@Component
public class ExchangeFactory {

    public Exchange getExchange(Class clazz) {
        return (Exchange) Main.getContext().getBean(clazz);
    }

    public Exchange getExchange(String exchangeClassName) {
        if (PoloniexExchange.class.getName().equals(exchangeClassName)) {
            return Main.getContext().getBean(PoloniexExchange.class);
        }
        return null;

    }

}
