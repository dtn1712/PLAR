package com.plar.ctp.exchange;

import com.plar.ctp.domain.model.OrderBook;
import com.plar.ctp.domain.model.raw.Ticker;

import java.util.List;

public interface Exchange {

    List<Ticker> getTickers();

    List<OrderBook> getOrderBooks();

    Double getFee();

    String getExchangeUrl();
}
