package com.plar.ctp.exchange.impl;

import com.plar.ctp.domain.model.Order;
import com.plar.ctp.domain.model.OrderBook;
import com.plar.ctp.domain.model.raw.Ticker;
import com.plar.ctp.exchange.Exchange;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class PoloniexExchange implements Exchange {

    private String baseUrl;
    private String tickerApiUrl;
    private String orderBookApiUrl;
    private Double fee;

    public PoloniexExchange(String baseUrl, String tickerApiUrl, String orderBookApiUrl, Double fee) {
        this.baseUrl = baseUrl;
        this.tickerApiUrl = tickerApiUrl;
        this.orderBookApiUrl = orderBookApiUrl;
        this.fee = fee;
    }

    @Override
    public List<Ticker> getTickers() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            String data = restTemplate.getForObject(this.tickerApiUrl, String.class);
            JSONObject jsonObject = new JSONObject(data);

            List<Ticker> tickers = new ArrayList<>();
            Map<String, Object> mapData = jsonObject.toMap();
            for (Map.Entry<String, Object> entry : mapData.entrySet()) {
                Map<String, String> entryData = (Map<String, String>) entry.getValue();
                Ticker ticker = new Ticker();
                ticker.setExchange(this);
                ticker.setName(entry.getKey());
                ticker.setLast(Double.parseDouble(entryData.get("last")));
                ticker.setLowestAsk(Double.parseDouble(entryData.get("lowestAsk")));
                ticker.setHighestBid(Double.parseDouble(entryData.get("highestBid")));
                ticker.setPercentChange(Double.parseDouble(entryData.get("percentChange")));
                ticker.setBaseVolume(Double.parseDouble(entryData.get("baseVolume")));
                ticker.setQuoteVolume(Double.parseDouble(entryData.get("quoteVolume")));
                ticker.setHigh24Hr(Double.parseDouble(entryData.get("high24hr")));
                ticker.setLow24Hr(Double.parseDouble(entryData.get("low24hr")));
                tickers.add(ticker);
            }

            return tickers;
        } catch (Exception e) {
            log.error("Failed to get tickers", e);
        }
        return null;
    }

    @Override
    public List<OrderBook> getOrderBooks() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            String data = restTemplate.getForObject(this.orderBookApiUrl, String.class);
            JSONObject jsonObject = new JSONObject(data);

            List<OrderBook> orderBooks = new ArrayList<>();
            Map<String, Object> mapData = jsonObject.toMap();
            for (Map.Entry<String, Object> entry : mapData.entrySet()) {
                Map<String, Object> entryData = (Map<String, Object>) entry.getValue();
                String[] currencies = entry.getKey().split("_");
                OrderBook orderBook = new OrderBook();
                orderBook.setTradeCurrency(currencies[0]);
                orderBook.setMainCurrency(currencies[1]);
                orderBook.setAsks(getOrders((List<List<Object>>) entryData.get("asks")));
                orderBook.setBids(getOrders((List<List<Object>>) entryData.get("bids")));
                orderBook.setLastUpdatedTime(DateTime.now());
                orderBooks.add(orderBook);
            }

            return orderBooks;
        } catch (Exception e) {
            log.error("Failed to get tickers", e);
        }
        return null;
    }

    private List<Order> getOrders(List<List<Object>> data) {
        List<Order> orders = new ArrayList<>();
        for (List<Object> ask : data) {
            Order order = new Order();
            order.setPrice(Double.parseDouble(String.valueOf(ask.get(0))));
            order.setQuantity(Double.parseDouble(String.valueOf(ask.get(1))));
            orders.add(order);
        }
        return orders;
    }

    @Override
    public Double getFee() {
        return fee;
    }

    @Override
    public String getExchangeUrl() {
        return this.baseUrl;
    }
}
