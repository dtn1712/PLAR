package com.plar.ctp.consumer;

import com.plar.ctp.domain.dao.ArbitrageDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ArbitrageDataAnalyzerConsumer {

    @Autowired
    private ArbitrageDAO arbitrageDAO;

    public void onMessage(String message) {

    }
}
