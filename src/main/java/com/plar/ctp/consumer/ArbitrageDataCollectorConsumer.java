package com.plar.ctp.consumer;

import com.fasterxml.jackson.core.type.TypeReference;
import com.plar.ctp.domain.dao.ArbitrageDAO;
import com.plar.ctp.domain.dto.ArbitrageDTO;
import com.plar.ctp.domain.model.filtered.Arbitrage;
import com.plar.ctp.util.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ArbitrageDataCollectorConsumer {

    @Autowired
    private ArbitrageDAO arbitrageDAO;

    public void onMessage(String message) {
        try {
            List<ArbitrageDTO> arbitrageDTOs = MapperUtils.getObjectMapper().readValue(message, new TypeReference<List<ArbitrageDTO>>(){});
            List<Arbitrage> arbitrages = arbitrageDTOs.stream().map(ArbitrageDTO::toModel).collect(Collectors.toList());
            arbitrageDAO.createArbitrages(arbitrages);
        } catch (IOException e) {
            log.error("Failed to read message", e);
        }
    }


}
