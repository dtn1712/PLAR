package com.plar.ctp;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 1/8/18
 * Time: 5:39 PM
 */
public class Constants {

    public static final String CACHE_KEY_SEPARATOR = ":::";

    public static final String BASE_COIN = "BTC";
    public static final int MAX_TOTAL_ARBITRAGE_CHOICES = 5;

}
