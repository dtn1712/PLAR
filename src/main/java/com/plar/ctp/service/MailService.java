package com.plar.ctp.service;

import com.plar.ctp.util.EncryptionUtils;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class MailService {

    private String encryptionKey;
    private String encryptedPassword;
    private String emailAccount;

    public MailService(String encryptionKey, String encryptedPassword, String emailAccount) {
        this.encryptionKey = encryptionKey;
        this.encryptedPassword = encryptedPassword;
        this.emailAccount = emailAccount;
    }

    public void sendEmail(String toEmail, String subject, String text) throws MessagingException {
        Session session = getSession(emailAccount, encryptedPassword);
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(emailAccount));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(toEmail));
        message.setSubject(subject);
        message.setText(text);

        Transport.send(message);

    }

    private Session getSession(String emailAccount, String encryptedPassword) {
        return javax.mail.Session.getInstance(getProperties(),
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(emailAccount, new String(EncryptionUtils.decrypt(encryptionKey.getBytes(), encryptedPassword.getBytes())));
                }
            });
    }

    private Properties getProperties() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        return props;
    }
}
