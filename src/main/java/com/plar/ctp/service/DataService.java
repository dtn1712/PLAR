package com.plar.ctp.service;

import com.plar.ctp.domain.model.OrderBook;
import com.plar.ctp.domain.model.filtered.Arbitrage;
import com.plar.ctp.domain.model.filtered.ArbitragePair;
import com.plar.ctp.domain.model.raw.Ticker;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DataService {

    public List<ArbitragePair> createArbitragePairs(List<Ticker> tickers) {
        Set<String> tickerSet = new HashSet<>();
        List<ArbitragePair> pairs = new ArrayList<>();
        for (Ticker ticker : tickers) {
            String[] currencies = ticker.getName().split("_");
            if (!tickerSet.contains(ticker.getName())) {
                pairs.add(new ArbitragePair(currencies[1].toUpperCase(), currencies[0].toUpperCase(), ticker.getLowestAsk(), ticker.getExchange()));
                pairs.add(new ArbitragePair(currencies[0].toUpperCase(), currencies[1].toUpperCase(), 1 / ticker.getHighestBid(), ticker.getExchange()));

                tickerSet.add(currencies[0].toUpperCase() + "_" + currencies[1].toUpperCase());
                tickerSet.add(currencies[1].toUpperCase() + "_" + currencies[0].toUpperCase());
            }
        }
        return pairs;
    }

    public void setupArbitrageOrderBooks(List<Arbitrage> arbitrages, List<OrderBook> orderBooks) {
        Map<String, OrderBook> orderBookMap = new HashMap<>();
        for (OrderBook orderBook : orderBooks) {
            orderBookMap.put(orderBook.getMainCurrency() + "_" + orderBook.getTradeCurrency(), orderBook);
            orderBookMap.put(orderBook.getTradeCurrency() + "_" + orderBook.getMainCurrency(), orderBook);
        }

        for (Arbitrage arbitrage : arbitrages) {
            Map<ArbitragePair, OrderBook> arbitragePairOrderBooks = (arbitrage.getOrderBooks() != null) ? arbitrage.getOrderBooks() : new HashMap<>();
            arbitrage.getPairs().stream().filter(arbitragePair -> orderBookMap.containsKey(arbitragePair.getCurrencyBuy() + "_" + arbitragePair.getCurrencySell())).forEach(arbitragePair -> {
                OrderBook orderBook = orderBookMap.get(arbitragePair.getCurrencyBuy() + "_" + arbitragePair.getCurrencySell());
                arbitragePairOrderBooks.put(arbitragePair, orderBook);
            });
            arbitrage.setOrderBooks(arbitragePairOrderBooks);
            arbitrage.setLastUpdatedTime(DateTime.now());
        }
    }
}
