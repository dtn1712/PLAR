package com.plar.ctp.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.api.sync.RedisCommands;
import com.plar.ctp.Constants;
import com.plar.ctp.util.MapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class CacheService {

    @Autowired
    private RedisClient redisClient;

    private RedisCommands<String, String> redisCommands;

    public void set(String key, String value) {
        RedisCommands<String, String> redisCommands = getRedisCommands();
        redisCommands.set(key, value);
    }

    public void set(String key, Object value) throws JsonProcessingException {
        RedisCommands<String, String> redisCommands = getRedisCommands();
        ObjectMapper mapper = MapperUtils.getObjectMapper();
        String objectData = mapper.writeValueAsString(value);
        redisCommands.set(key, objectData);
    }

    public String get(String key) {
        RedisCommands<String, String> redisCommands = getRedisCommands();
        return redisCommands.get(key);
    }

    public Object get(String key, Class clazz) throws IOException {
        RedisCommands<String, String> redisCommands = getRedisCommands();
        ObjectMapper mapper = MapperUtils.getObjectMapper();
        String redisData = redisCommands.get(key);
        return mapper.readValue(redisData,clazz);
    }

    public Object getOrSet(String key, Object value, Class clazz) throws IOException {
        RedisCommands<String, String> redisCommands = getRedisCommands();
        ObjectMapper mapper = MapperUtils.getObjectMapper();
        String redisData = redisCommands.get(key);
        if (redisData == null) {
            String objectData = mapper.writeValueAsString(value);
            redisCommands.set(key, objectData);
            return value;
        } else {
            return mapper.readValue(redisData,clazz);
        }
    }

    public String getOrSet(String key, String value) {
        RedisCommands<String, String> redisCommands = getRedisCommands();
        String redisData = redisCommands.get(key);
        if (redisData == null) {
            redisCommands.set(key, value);
            return value;
        } else {
            return redisData;
        }
    }

    public void delete(String key) {
        RedisCommands<String, String> redisCommands = getRedisCommands();
        redisCommands.del(key);
    }

    public void deletePattern(String keyPattern) {
        RedisCommands<String, String> redisCommands = getRedisCommands();
        List<String> keys = redisCommands.keys(keyPattern);
        keys.forEach(this::delete);
    }

    public boolean ping() {
        RedisCommands<String, String> redisCommands = getRedisCommands();
        return redisCommands.isOpen();
    }

    public String generateCacheKey(Class clazz, String... args) {
        if (clazz == null || args == null || args.length > 0) {
            return null;
        }
        String result = clazz.getName() + Constants.CACHE_KEY_SEPARATOR;
        for (String arg : args) {
            result = result + arg + Constants.CACHE_KEY_SEPARATOR;
        }
        return result.substring(0, result.length() - Constants.CACHE_KEY_SEPARATOR.length());
    }

    private RedisCommands<String, String> getRedisCommands() {
        if (redisCommands == null) {
            redisCommands = redisClient.connect().sync();
        }
        return redisCommands;
    }
}
