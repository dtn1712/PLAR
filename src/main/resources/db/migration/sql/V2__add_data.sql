INSERT INTO `jobs` (`id`, `key`, `className`, `jobName`, `description`, `isActive`, `schedule`, `group`, `trigger`, `createdAt`, `updatedAt`)
VALUES
	(1, 'findArbitrage', 'com.plar.ctp.job.FindArbitrageJob', 'findArbitrageJob', 'Find Arbitrage Every Five Second', 1, '*/5 * * * * ?', 'everyFiveSecond', 'findArbitrageTrigger', '2017-12-22 14:16:02', '2017-12-22 14:16:02');
