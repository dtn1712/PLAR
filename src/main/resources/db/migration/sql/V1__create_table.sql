CREATE TABLE `jobs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) NOT NULL,
  `className` varchar(300) NOT NULL,
  `jobName` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active flag',
  `schedule` varchar(50) NOT NULL,
  `group` varchar(50) NOT NULL,
  `trigger` varchar(50) NOT NULL,
  `createdAt` datetime DEFAULT NULL COMMENT 'timestamp when created this record',
  `updatedAt` datetime DEFAULT NULL COMMENT 'timestamp when updated this record',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


CREATE TABLE `arbitrages` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `rate` DECIMAL(30,20) NOT NULL,
  `exchange` varchar(100) NOT NULL,
  `createdAt` datetime DEFAULT NULL COMMENT 'timestamp when created this record',
  `updatedAt` datetime DEFAULT NULL COMMENT 'timestamp when updated this record',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


CREATE TABLE `arbitrage_pairs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `currencyBuy` varchar(10) NOT NULL,
  `currencySell` varchar(10) NOT NULL,
  `arbitrageId` bigint(20) NOT NULL,
  `chainPosition` SMALLINT(2) NOT NULL,
  `rate` DECIMAL(30,20) NOT NULL,
  `rateAfterFee` DECIMAL(30,20) NOT NULL,
  `exchange` varchar(100) NOT NULL,
  `createdAt` datetime DEFAULT NULL COMMENT 'timestamp when created this record',
  `updatedAt` datetime DEFAULT NULL COMMENT 'timestamp when updated this record',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;